/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

/**
 *
 * @author João
 */
abstract class AbstractProduto {
    private String nome;
    private String idProd;
    private Float valor;
    private String subTipo;
    private int qtd;
    private String observacao; 
    
    public AbstractProduto(String idProd, String subTipo, String nome, Float valor, 
             int qtd, String observacao){
        this.nome = nome;
        this.idProd = idProd;
        this.valor = valor;
        this.subTipo = subTipo;
        this.qtd = qtd;
        this.observacao = observacao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdProd() {
        return idProd;
    }

    public void setIdProd(String idProd) {
        this.idProd = idProd;
    }

    public Float getValor() {
        return valor;
    }

    public void setValor(Float valor) {
        this.valor = valor;
    }

    public String getSubTipo() {
        return subTipo;
    }

    public void setSubTipo(String subTipo) {
        this.subTipo = subTipo;
    }

    public int getQtd() {
        return qtd;
    }

    public void setQtd(int qtd) {
        this.qtd = qtd;
    }
    
}
