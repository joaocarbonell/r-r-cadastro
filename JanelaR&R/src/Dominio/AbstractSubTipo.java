/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

/**
 *
 * @author João
 */
abstract class AbstractSubTipo {
    private String nome;
    private String codId;
    private String tipoProduto;

    public AbstractSubTipo(String codId, String tipProduto, String nome) {
        this.nome = nome;
        this.codId = codId;
        this.tipoProduto = tipProduto;
    }

    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCodId() {
        return codId;
    }

    public void setCodId(String codId) {
        this.codId = codId;
    }

    public String getTipoProduto() {
        return tipoProduto;
    }

    public void setTipoProduto(String tipoProduto) {
        this.tipoProduto = tipoProduto;
    }
    
}
