/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dominio;

import java.util.ArrayList;

/**
 *
 * @author João
 */
abstract class AbstractTipoProduto {
    private String nome;
    private String idTipo;
    private String observacoes;
    

    public AbstractTipoProduto(String idTipo,String nome, String observacoes){
        this.nome = nome;
        this.idTipo = idTipo;
        this.observacoes = observacoes;
        
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(String idTipo) {
        this.idTipo = idTipo;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }

   
   
  
}
