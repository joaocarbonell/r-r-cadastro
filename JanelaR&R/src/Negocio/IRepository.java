/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author João
 */
public interface IRepository {
 
    public void create(Object[] obj);
       
    public void delete(int id);
        
    public void pesquisa(String id);
    
    public ArrayList retrieve(ResultSet rs);
    
    public String geraCod();
}
