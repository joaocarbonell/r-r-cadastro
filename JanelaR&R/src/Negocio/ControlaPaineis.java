/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dominio.*;
import InterfacesGraficas.*;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author João
 */
public class ControlaPaineis {

    private JanelaCadastrarProduto janelaCadastrarProduto;
    private JanelaCadastrarTipo janelaCadastrarTipo;
    private JanelaCadastrarSubTipo janelaCadastrarSubTipo;
    private static ControlaPaineis instancia = null;
    private JanelaPrincipal janelaPrincipal;

    public static ControlaPaineis getInstancia() {
        if (instancia == null) {

            instancia = new ControlaPaineis();
        }
        return instancia;
    }

    public ControlaPaineis() {

    }

    public void fecharSistemaX(java.awt.event.WindowEvent evt) {

        if (evt.getID() == WindowEvent.WINDOW_CLOSING) {
            int selectedOption = JOptionPane.showConfirmDialog(null, "Deseja Sair Realmente?",
                    "Sistema informa:", JOptionPane.YES_NO_OPTION);

            if (selectedOption == JOptionPane.YES_OPTION) {

                System.exit(0);
            }
        }
    }

    public void fecharSistemaSair() {

        int selectedOption = JOptionPane.showConfirmDialog(null, "Deseja Sair Realmente?",
                "Confirmação de saida", JOptionPane.YES_NO_OPTION);

        if (selectedOption == JOptionPane.YES_OPTION) {

            System.exit(0);
        }
    }

    public void fechaJanelasCadastro(java.awt.event.ActionEvent evt, JFrame jfr) {

        Object[] botoes = {"Sim", "Não"};
        int resposta = JOptionPane.showOptionDialog(null,
                "Deseja mesmo sair do cadastro? Se já preenchidas, todas as "
                + "informações serão perdidas",
                "Confirmação", // o título da janela  
                JOptionPane.DEFAULT_OPTION, JOptionPane.QUESTION_MESSAGE, null,
                botoes, botoes[0]);

        if (resposta == 0) {
            jfr.dispose();
            janelaPrincipal.setEnabled(true);
            janelaPrincipal.setVisible(true);

        } else if (resposta == 1) {

        }

    }

    public void criarJanelaCadastroProdutos() {
        JanelaCadastrarProduto janelaCadastrarProduto = new JanelaCadastrarProduto();
        this.janelaCadastrarProduto = janelaCadastrarProduto;
        janelaCadastrarProduto.setVisible(true);
        janelaPrincipal.setEnabled(false);
    }

    public void criarJanelaCadastroTipo() {
        JanelaCadastrarTipo janelaCadastrarTipo = new JanelaCadastrarTipo();
        this.janelaCadastrarTipo = janelaCadastrarTipo;
        janelaCadastrarTipo.setVisible(true);
        janelaPrincipal.setEnabled(false);
    }

    public void criarJanelaCadastroSubTipo() {
        JanelaCadastrarSubTipo janelaCadastrarSubTipo = new JanelaCadastrarSubTipo();
        this.janelaCadastrarSubTipo = janelaCadastrarSubTipo;
        janelaCadastrarSubTipo.setVisible(true);
        janelaPrincipal.setEnabled(false);

    }

    public void criarJanelaPrincipal(ControladoraFluxo ctrFluxo) {
        JanelaPrincipal janelaPrincipal = new JanelaPrincipal(ctrFluxo);
        this.janelaPrincipal = janelaPrincipal;
        Login.getInstance().dispose();
        janelaPrincipal.setVisible(true);

    }

    public void mensagemSucessTipoProd() {
        JOptionPane.showMessageDialog(null, "Cadastro realizado com sucesso!");
        janelaCadastrarTipo.dispose();
        criarJanelaCadastroTipo();
    }

    public void mensagemSucessProd() {
        JOptionPane.showMessageDialog(null, "Cadastro realizado com sucesso!");
        janelaCadastrarProduto.dispose();
        criarJanelaCadastroProdutos();
    }

    public void mensagemSucessSub() {
        JOptionPane.showMessageDialog(null, "Cadastro realizado com sucesso!");
        janelaCadastrarSubTipo.dispose();
        criarJanelaCadastroSubTipo();
    }

   public void atualizaTabelaProdutos(){
//       try {
//           
//       
////       ArrayList<Produto> array;
////       array = ControladoraFluxo.getInstancia().buscaTodosProdutos();
////       for(int i = 0; i<array.size(); i++){
//         
////           
////           janelaPrincipal.getjTableTabelaProduto().
////              DefaultTableModel modelo = new DefaultTableModel(); 
////                JTable tabela = new JTable(modelo);
////                
////                String idTipo = array.get(i).getSubTipo();
////       modelo.addRow(newObject[]{array.get(i).getIdProd(), array.get(i).getNome(), array.get(i).get});
//               
//       
//       }
//        } catch (Exception e) {
//            JOptionPane.showMessageDialog(null, e);
//      }               
   }
   
    public void atualizaTipos(ArrayList<TipoProduto> tipos) {
        try {
            int cont1 = 0, cont2 = 0;
            String str;
            String str2;

            for (int i = 0; i < tipos.size(); i++) {

                str = tipos.get(i).getNome();

                for (int j = 0; j < janelaPrincipal.getjComboBoxFiltroTipo().getItemCount(); j++) {
                    str2 = janelaPrincipal.getjComboBoxFiltroTipo().getItemAt(j).toString();
                    if (str2.equals(str)) {
                        System.out.println(str2);
                        cont1++;
                    } else {

                        cont2++;
                    }
                }
                if (cont1 == 0) {

                    janelaPrincipal.getjComboBoxFiltroTipo().addItem(str);
                }
                cont1 = 0;
                cont2 = 0;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.out.println(e.getCause());
            System.out.println(e.toString());
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, JOptionPane.ERROR_MESSAGE);
        }

    }

    public void atualizaTiposOnStart(ArrayList<TipoProduto> tipos) {
        try {

            String str = "";
            for (int j = 0; j < janelaPrincipal.getjComboBoxFiltroTipo().getItemCount(); j++) {
                janelaPrincipal.getjComboBoxFiltroTipo().removeItemAt(j);
            }
            for (int i = 0; i < tipos.size(); i++) {

                str = tipos.get(i).getNome();

                janelaPrincipal.getjComboBoxFiltroTipo().addItem(str);

            }

        } catch (Exception e) {

            JOptionPane.showMessageDialog(null, JOptionPane.ERROR_MESSAGE);
        }

    }
    public String retornaTipo(String idTipo){
        String ids = ""; 
       
        ArrayList<TipoProduto> list;
         list = ControladoraFluxo.getInstancia().buscaTodosTiposProdutos();
       
          for(int j = 0 ; j< list.size();j++){
                if(idTipo.equals(list.get(j).getNome())){
                    ids = list.get(j).getIdTipo();  
                }
    }
         
          return ids;       
    }
    public void atualizaSubTipos(ArrayList<SubTipo> subTipos, String idTipo) {
        try {
            janelaPrincipal.getjComboBoxFiltroSubTipo().removeAllItems();
           
            String str = "";
            String id = "";
                   id = this.retornaTipo(idTipo);

            for (int i = 0; i < subTipos.size(); i++) {
               
                System.out.println(id);
                  if(subTipos.get(i).getTipoProduto().equals(id)){//Necessario converter os valores das strings em cod do tipo
                str = subTipos.get(i).getNome();
                     
                janelaPrincipal.getjComboBoxFiltroSubTipo().addItem(str);
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print(e.getMessage());
            System.out.println(e.getCause());
            JOptionPane.showMessageDialog(null, JOptionPane.ERROR_MESSAGE);
        }

    }

    
}
