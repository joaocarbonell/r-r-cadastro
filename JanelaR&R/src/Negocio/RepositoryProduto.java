/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dominio.Produto;
import Persistencia.DAOProduto;
import java.sql.*;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JOptionPane;

/**
 *
 * @author João
 */
public class RepositoryProduto implements IRepository{

    
    private final ArrayList<Produto> arrayProdutos;
    private DAOProduto daoProduto;
    
    
    public RepositoryProduto(){
        arrayProdutos = new ArrayList<>();
        daoProduto = new DAOProduto();
        retrieve(daoProduto.todos());
        
    }
    
    @Override
    public void create(Object[] obj) {
      
        
        try {                 
        
        String cod = geraCod();
            obj[0] = cod;
        
            
        daoProduto.insert(obj);
            retrieve(daoProduto.todos());
            
        ControladoraFluxo.getInstancia().cadastroProdutoRealizado();
          
        } catch (Exception e) {
        
            JOptionPane.showMessageDialog(null, e, null, JOptionPane.ERROR_MESSAGE);
            
        }
        
    }
public void createBD(Object[] obj){
    
       
        Produto produto = new Produto(obj[3].toString(), obj[1].toString(),
                Float.valueOf(obj[4].toString()), obj[2].toString(), 
                Integer.parseInt(obj[5].toString()), obj[6].toString());

        arrayProdutos.add(produto);
    
}
    @Override
    public void delete(int id) {
        
    }
   
    @Override
    public String geraCod(){
       
         String str = "";
        
        try {
            
       
            str = String.valueOf(arrayProdutos.size()+1);
       
        
        } catch (Exception e) {
        }
        
        return str;
    }
    
    @Override
    public void pesquisa(String id){
        
    }
    
     public ArrayList buscaArray() {
        return arrayProdutos;
    }

    @Override
    public ArrayList retrieve(ResultSet rs) {
        
      ArrayList<Produto> arrayProduto = null;
        try {
           arrayProduto = new ArrayList();
            while(rs.next()){
                Object obj[] = {rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),
                    rs.getString(5),rs.getString(6),rs.getString(7),};
               this.createBD(obj);
                
                
            }
            return arrayProduto;
        } catch (Exception e) {
        return arrayProduto;
        }
    }
    
    
    
}
