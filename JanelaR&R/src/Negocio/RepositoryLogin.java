/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;


import Dominio.UsuarioLogin;
import Persistencia.DAOLogin;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 *
 * @author João
 */
public class RepositoryLogin implements IRepository{

    private final ArrayList<UsuarioLogin> arrayUsuarioLogin;
    private final DAOLogin dAOLogin;

    public RepositoryLogin() {
        
        arrayUsuarioLogin = new ArrayList<>();
        dAOLogin = new DAOLogin();
        retrieve(dAOLogin.todos());
        
        
    }
    
    
    
    @Override
    public void create(Object[] obj) {
        
        UsuarioLogin usuarioLogin = new UsuarioLogin(obj[0].toString(), obj[1].toString()); 
        arrayUsuarioLogin.add(usuarioLogin);
        
        
    }

    @Override
    public void delete(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void pesquisa(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    public ArrayList buscaArray() {
        return arrayUsuarioLogin;
    }

    @Override
    public String geraCod() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ResultSet todos(){
        return dAOLogin.todos();
    }
    
    
 @Override
    public ArrayList retrieve(ResultSet rs){
        
       ArrayList<UsuarioLogin> arrayUsuarioLogin = null;
        try {
           arrayUsuarioLogin = new ArrayList();
            while(rs.next()){
                Object obj[] = {rs.getString(2),rs.getString(3)};
               this.create(obj);
                
                
            }
            return arrayUsuarioLogin;
        } catch (Exception e) {
        return arrayUsuarioLogin;
        }
        
    }
    
}
