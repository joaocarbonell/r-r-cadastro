/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dominio.UsuarioLogin;
import Persistencia.ConectaDB;
import Persistencia.DAOLogin;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;



/**
 *
 * @author João
 */
public class ControladoraCadastro {

    private static ControladoraCadastro instancia = null;
    private ControlaPaineis controladorPainel;
    private final RepositoryProduto repositoryProduto;
    private final RepositorySubTipo repositorySubTipo;
    private final RepositoryTipoProduto repositoryTipo;
    private final RepositoryLogin repositoryLogin;
    private ResultSet rs;
    

    public static ControladoraCadastro getInstancia() {
        if (instancia == null) {
            instancia = new ControladoraCadastro();
        }
        return instancia;
    }

    public ControladoraCadastro() {
        
       ConectaDB.conectaDB();
        repositoryProduto = new RepositoryProduto();
        repositorySubTipo = new RepositorySubTipo();
        repositoryTipo = new RepositoryTipoProduto();
        repositoryLogin = new RepositoryLogin();
        
        
         
        
        

    }
    public String retriveSubTipo(String nomeSubTipo){
        try {
            
       
        for(int i = 0; i < repositorySubTipo.retornaArraySubTipo().size(); i++){
         if(nomeSubTipo.equals(repositorySubTipo.retornaArraySubTipo().get(i).getNome())){   
       
             return repositorySubTipo.retornaArraySubTipo().get(i).getCodId();
        }
        }
        
         } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
         }
        return null;
    }

    public void createProduto(Object[] obj){
      
        String idSubTipo = obj[1].toString();
        try {
        
        retriveSubTipo(idSubTipo);
        
        obj[1] = idSubTipo;
        repositoryProduto.create(obj);
        
            
        } catch (Exception e) {
            
            JOptionPane.showMessageDialog(null, e.getMessage());
        }
        
    }
    
    public void createTipo(Object[] obj){
        
        repositoryTipo.create(obj);
        
    }
    
    public void createSubTipo(Object[] obj){
        
       repositorySubTipo.create(obj);
        
    }
    
    public void cadastroRealizado(){
        ControladoraFluxo.getInstancia().cadastroSubTipoRealizado();
    }
    
    public ArrayList retornaArrayUsuarios(){
        
        return repositoryLogin.buscaArray();
    }
    
     public ArrayList retornaArrayProdutos(){
         
         return repositoryProduto.buscaArray();
     }
     
     public ArrayList retornaArrayTipos(){
         
         return repositoryTipo.buscaArray();
     }
     
     public ArrayList retornaArraySubTipos(){
         
         return repositorySubTipo.retornaArraySubTipo();
     }
    
    public ResultSet todosLogins(){
        
     return repositoryLogin.todos();
        
    }
}
