/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;




/**
 *
 * @author João
 */
public class Inicializador {
    
    private ControladoraFluxo controladorFluxo;

    public Inicializador(ControladoraFluxo controladorFluxo) {
       
        this.controladorFluxo = controladorFluxo;
        this.iniciarSistema();
        
    }
    
    public void iniciarSistema(){
        controladorFluxo.login();
        
    }
    
    public static void main(String args[]){
       
        ControladoraFluxo controladoraFluxo = new ControladoraFluxo();
        Inicializador iniciador = new Inicializador(controladoraFluxo);
        
    }
            
}
