/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Dominio.UsuarioLogin;
import InterfacesGraficas.JanelaCadastrarProduto;

import InterfacesGraficas.Login;

import java.sql.*;
import Persistencia.ConectaDB;
import Persistencia.DAOLogin;
import java.util.ArrayList;
import javax.swing.JOptionPane;


/**
 *
 * @author João
 */
public class ControladoraFluxo{
    private ControladoraCadastro controladoraCadastro;
    private ControlaPaineis controlaPaineis;
   
    private boolean logado = false;
    private JanelaCadastrarProduto janelaCadastrarProduto;
    private static ControladoraFluxo instancia= null;
    
   
     public static ControladoraFluxo getInstancia() {
        if (instancia == null) {
            instancia = new ControladoraFluxo();
        }
        return instancia;
    }

    public ControladoraFluxo() {
        controladoraCadastro = ControladoraCadastro.getInstancia();
        controlaPaineis = ControlaPaineis.getInstancia();
       
        
       
        
       
        
    }
    
    public void login(){
        Login.getInstance().setVisible(true);
         
    }
    
    public void inicalizarSistema(){
        
        ControlaPaineis.getInstancia().criarJanelaPrincipal(this);
         this.atualizaPaineisTipoInicio();
         controlaPaineis.atualizaTabelaProdutos();
         
       
        
        
            }
    
    public void lancaJanelaCadastroProduto(){
        controlaPaineis.criarJanelaCadastroProdutos();
        
        
    }
    
    public void lancaJanelaCadastroTipo(){
        controlaPaineis.criarJanelaCadastroTipo();
        
        
    }
    
    public void lancaJanelaCadastroSubTipo(){
        controlaPaineis.criarJanelaCadastroSubTipo();
        
        
    }
    
    public void autentica(String usuario, String senha){
        
        this.autenticaValida(usuario, senha);
        
    }
   
    
    private void autenticaValida(String usuario, String senha){//Método sendo implementado de forma provisória
     
       
     ArrayList<UsuarioLogin> arrayUsuario;
     
        try {
            
            
          
             arrayUsuario = controladoraCadastro.retornaArrayUsuarios();
                
        
            try {
               
                for(int i =0; i<arrayUsuario.size(); i++){
                    
                    if(arrayUsuario.get(i).getNome().equals(usuario)){
                        
                        if(arrayUsuario.get(i).getSenha().equals(senha)){
                             
                            this.inicalizarSistema();
                            
                        }else{
                            JOptionPane.showMessageDialog(null, "Usuario ou senha errada!");
                           
                            
                            this.login();
                            
                        }
                    }else{
                         JOptionPane.showMessageDialog(null, "Usuario ou senha errada!");
                            
                            
                            this.login();
                            

                    }
                }
                
            } catch (Exception e) {
           JOptionPane.showMessageDialog(null, e);
            
            }      
        
         } catch (Exception e) {
             JOptionPane.showMessageDialog(null, e);
             
        }
        
        
        
    }
    
    public void fechaSistemaX(java.awt.event.WindowEvent evt){
        controlaPaineis.fecharSistemaSair();
    }
    
    public void fechaSistemaSair(){
        controlaPaineis.fecharSistemaSair();
    }
    
    public void createProduto(Object[] obj){
        
        controladoraCadastro.createProduto(obj);
//        controlaPaineis.atualizaTabelaProdutos();
        
    }
   
    public void createTipoProduto(Object[] obj){
        
        controladoraCadastro.createTipo(obj);
        
        
    }
    
    public void createSubTipo(Object[] obj){
        
        controladoraCadastro.createSubTipo(obj);
        
    }
    
    public void cadastroTipoRealizado(){
        controlaPaineis.mensagemSucessTipoProd();
    }
    
    public void cadastroProdutoRealizado(){
        controlaPaineis.mensagemSucessProd();
    }
    
    public void cadastroSubTipoRealizado(){
        controlaPaineis.mensagemSucessProd();
    }
    
    public ArrayList buscaTodosProdutos(){
        return controladoraCadastro.retornaArrayUsuarios();
                
    }
    
    public ArrayList buscaTodosTiposProdutos(){
        return controladoraCadastro.retornaArrayTipos();
        
    }
    
    public ArrayList buscaTodosSubTipos(){
        return controladoraCadastro.retornaArraySubTipos();
        
    }
    public void atualizaPaineisTipoInicio(){
 
        controlaPaineis.atualizaTiposOnStart(buscaTodosTiposProdutos());
     
    }
    
    public void atualizaPaineisTipo(){
        controlaPaineis.atualizaTipos(buscaTodosTiposProdutos());
    }
    
    
    public void atualizaPaineisSubTipo(String idTipo){
        controlaPaineis.atualizaSubTipos(buscaTodosSubTipos(),idTipo);
    }
}
            