/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author João
 */
public interface IDAO {
    
    
    public void insert(Object[] obj);
    
    public void delete(String id);
        
    public ArrayList select(String id);
    
    public void update(String id);
    
    public ResultSet todos();
    
    
}
