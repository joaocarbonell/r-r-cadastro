/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author João
 */
public class DAOTipo implements IDAO{
    
    private PreparedStatement stm = null;
    private ResultSet rs = null;
    
    
    public DAOTipo(){
        this.todos();
    }

    @Override
    public void insert(Object[] obj) {
        String sql = "insert into tipoproduto values(?,?,?)";
        
        try {
            
      
        stm = ConectaDB.getInstancia().getConexao().prepareStatement(sql);
        stm.setString(1, obj[0].toString());
        stm.setString(2, obj[1].toString());
        stm.setString(3, obj[2].toString());
        
        stm.executeUpdate();
      
                
                  } catch (SQLException e) {
                      
                      JOptionPane.showMessageDialog(null, e+" Contate o administrador do sistema!");
                     
                      
        }
    }

    @Override
    public void delete(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList select(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void update(String id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ResultSet todos() {
          String sql = "select * from tipoproduto";
        try {
      
        stm = ConectaDB.getInstancia().getConexao().prepareStatement(sql);
        rs = stm.executeQuery();
            
        return rs;
                
                  } catch (SQLException e) {
                      
                      JOptionPane.showMessageDialog(null, e+" Contate o administrador do sistema!");
                     
                      return rs;
        }
    }
    
}
