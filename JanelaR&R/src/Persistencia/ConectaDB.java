/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Persistencia;

import java.sql.*;
import java.sql.DriverManager;
import javax.swing.JOptionPane;

/**
 *
 * @author João
 */
public class ConectaDB {
    private static Connection con;
    private static ConectaDB instancia;
    
    public static ConectaDB getInstancia(){
        if(instancia == null){
            instancia = new ConectaDB();
        }
        return instancia;
        
    }
    
    public static Connection conectaDB() {
        
        try {
            
            Class.forName("org.postgresql.Driver");
            
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/sci", 
                    "postgres", "j1b2p3c4");
                         
            JOptionPane.showMessageDialog(null, "Conectado");

            return con;
            
            
            
        } catch (Exception error) {
            
            JOptionPane.showMessageDialog(null, error, null, JOptionPane.ERROR_MESSAGE);
            
            return null;
        }
        
        
        
    }
   
    
    
    public Connection getConexao(){
        
            return con;
            
    }
    
}
